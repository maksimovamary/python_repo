create table if not exists authors (
	author_id integer PRIMARY KEY autoincrement,
	author_name string  NOT NULL
);
create table if not exists books (
	book_id integer PRIMARY KEY autoincrement,
	book_title string NOT NULL
);	

create table if not exists authors_books ( 
	id integer PRIMARY KEY autoincrement,
    author_name string not null references authors(author_name),
    book_title string not null references books(book_title)
)

