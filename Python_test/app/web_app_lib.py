import sqlite3
import os
from flask import Flask, request, session, g, redirect, url_for, \
	abort, render_template, flash
from login_form import LoginForm	

from contextlib import closing	

DATABASE = '/tmp/web_lib.db'
DEBUG = True
SECRET_KEY = 'development key'
USERNAME = 'adm'
PASSWORD = '7049'

app = Flask(__name__)
app.config.from_object('config')

app.config.update(dict(
	DATABASE=os.path.join(app.root_path, 'web_lib.db'),
	DEBUG = True,
	SECRET_KEY='development key',
	USERNAME='adm',
	PASSWORD='7049'
))
app.config.from_envvar('WEBLIB_SETTINGS', silent=True)
	
def connect_db():
	rv = sqlite3.connect(app.config['DATABASE'])
	rv.row_factory = sqlite3.Row
	return rv
		
def get_db():
	if not hasattr(g, 'sqlite.db'):
		g.sqlite_db = connect_db()
	return g.sqlite_db
	
@app.teardown_appcontext
def close_db(error):
	if hasattr(g, 'sqlite_db'):
		g.sqlite_db.close()
		
def init_db():
	with app.app_context():
		db = get_db()
		with app.open_resource('schema.sql', mode = 'r') as f:
			db.cursor().executescript(f.read())
		db.commit()	
		with app.open_resource('data.sql', mode = 'r') as f:
			db.cursor().executescript(f.read())
		db.commit()	
		
def execute_db(select_text,db):
	cur = db.execute(select_text);
	return cur.fetchall()	
		
		
@app.route('/')
def show_main():
	db = get_db()
	authors_books = execute_db('''select book_title, author_name FROM authors_books order by book_title''', db)
	books = execute_db('''select book_title FROM books order by book_title''', db)
	authors = execute_db('''select author_name FROM authors order by author_name''', db)
	return render_template('show_main.html', authors_books=authors_books, authors=authors, books=books )				

@app.route('/show_books')
def show_books():
	db = get_db()
	cur = db.execute('''select book_title FROM books order by book_title''')
	books = cur.fetchall()
	return render_template('show_books.html', books=books)

@app.route('/find_in_lib', methods=['GET'])
def find_in_lib():
	db = get_db()
	author_name = request.args.get('findAuthorName')
	book_title = request.args.get('findBookTitle')
	cur = db.execute('''select author_name, book_title  FROM authors_books where author_name = ? or book_title = ? ''', 
					(author_name, book_title))
	authors_books = cur.fetchall()
	books = execute_db('''select book_title FROM books order by book_title''', db)
	authors = execute_db('''select author_name FROM authors order by author_name''', db)
	return render_template('show_main.html', authors_books=authors_books, authors=authors, books=books)		

@app.route('/show_authors')
def show_authors():
	db = get_db()
	cur = db.execute('''select author_name FROM authors order by author_name''')
	authors = cur.fetchall()
	return render_template('show_authors.html', authors=authors)

@app.route('/delete_author/<author_name>', methods=['GET'])
def delete_author(author_name):
	if not session.get('logged_in'):
		abort(401)
	db = get_db()
	db.execute('''delete from authors where author_name = ? ''', (author_name,))
	db.commit()
	flash('Author was deleted')
	return redirect(url_for('show_authors'))
	
@app.route('/delete_book/<book_title>', methods=['GET'])
def delete_book(book_title):
	if not session.get('logged_in'):
		abort(401)
	db = get_db()
	db.execute('''delete from books where book_title = ? ''', (book_title,))
	db.commit()
	flash('Book was deleted')
	return redirect(url_for('show_books'))	
	
@app.route('/delete_from_lib/<author_name>,<book_title>', methods=['GET'])
def delete_from_lib(author_name, book_title):
	if not session.get('logged_in'):
		abort(401)
	db = get_db()
	db.execute('''delete from authors_books where author_name = ? and book_title = ?''', (author_name,book_title))
	db.commit()
	flash('Deleted')
	return redirect(url_for('show_main'))	
	
@app.route('/add_author', methods=['POST'])
def add_author():
	if not session.get('logged_in'):
		abort(401)
	db = get_db()	
	db.execute('insert into authors (author_name) values (?)',
				[request.form['author_name']])
	db.commit()
	flash('New author was successfully added')
	return redirect(url_for('show_authors'))

@app.route('/update_author/<author_name>', methods=['POST'])
def update_author(author_name):
	if not session.get('logged_in'):
		abort(401)
	db = get_db()	
	db.execute('''update authors set author_name = ? where author_name = ? ''',
				(request.form['new_author_name'],author_name))
	db.commit()
	flash('Author was successfully updated')
	return redirect(url_for('show_authors'))		
	
@app.route('/add_book', methods=['POST'])
def add_book():
	if not session.get('logged_in'):
		abort(401)
	db = get_db()	
	db.execute('insert into books (book_title) values (?)',
				[request.form['book_title']])
	db.commit()
	flash('New book was successfully added')
	return redirect(url_for('show_books'))
	
@app.route('/update_book/<book_title>', methods=['POST'])
def update_book(book_title):
	if not session.get('logged_in'):
		abort(401)
	db = get_db()	
	db.execute('''update books set book_title = ? where book_title = ? ''',
				(request.form['new_book_title'],book_title))
	db.commit()
	flash('The book was successfully updated')
	return redirect(url_for('show_books'))	

@app.route('/add_in_lib', methods=['POST'])
def add_in_lib():
	if not session.get('logged_in'):
		abort(401)
	db = get_db()	
	db.execute('''insert into authors_books (author_name, book_title) values (?,?)''',
				(request.form['select_author'],request.form['select_book']))
	db.commit()
	flash('Successfully added')
	return redirect(url_for('show_main'))		
	
@app.route('/login', methods=['GET','POST'])
def login():
	error = None
	form = LoginForm()
	if form.validate_on_submit():
		if form.username.data != app.config['USERNAME']:
			error = 'Invalid username'
		elif form.password.data	!= app.config['PASSWORD']:
			error = 'Invalid password'
		else:
			session['logged_in'] = True
			flash('You were logged in')
			return redirect(url_for('show_main'))
	return render_template('login.html', title = 'Sign In', form = form)

@app.route('/logout')
def logout():
	session.pop('logged_in', None)
	flash('You were logged out')
	return redirect(url_for('show_main'))
	