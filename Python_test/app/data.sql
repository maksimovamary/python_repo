DROP TABLE if exists web_lib.authors_books;
DROP TABLE if exists web_lib.books;
DROP TABLE if exists web_lib.authors;

insert into books (book_id,book_title) values (1,'Journey to the Center of the Earth');
insert into books (book_id,book_title) values (2,'From the Earth to the Moon');
insert into books (book_id,book_title) values (3,'Around the World in 80 Days');
insert into books (book_id,book_title) values (4, 'Queen Margot');
insert into books (book_id,book_title) values (5,'Three Musketeers');
insert into books (book_id,book_title) values (6,'The Count of Monte Cristo');
insert into books (book_id,book_title) values (7, 'Lolita');		

insert into authors (author_id,author_name) values (1,'Aleksandr Dumas');
insert into authors (author_id,author_name) values (2, 'Jul Verne');
insert into authors (author_id,author_name) values (3,'Nabokov');		
		
insert into authors_books ( author_name, book_title) values ('Aleksandr Dumas', 'Queen Margot');
insert into authors_books ( author_name, book_title) values ('Nabokov', 'Lolita');